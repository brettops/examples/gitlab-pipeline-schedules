locals {
  projects = {
    for project in data.gitlab_projects.brettops.projects : project.id => {
      id   = project.id
      name = project.path_with_namespace
      url  = project.web_url
      ref  = project.default_branch
    }
  }

  pipeline_schedules = {
    for project in local.projects : project.id => {
      project = project
      hour    = random_integer.hour[project.id].result
      weekday = random_integer.weekday[project.id].result
    }
  }

  schedule_page = [
    for hour in range(24) : {
      hour = format("%02s:00", hour)
      weekdays = [
        for weekday in range(7) : [
          for schedule in local.pipeline_schedules : {
            name = schedule.project.name
            url  = schedule.project.url
          } if schedule.hour == hour && schedule.weekday == weekday
        ]
      ]
    }
  ]
}
