data "gitlab_group" "brettops" {
  full_path = "brettops"
}

data "gitlab_projects" "brettops" {
  group_id          = data.gitlab_group.brettops.id
  include_subgroups = true
  with_shared       = false
  visibility        = "public"
}
