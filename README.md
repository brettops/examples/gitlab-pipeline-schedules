# gitlab-pipeline-schedules

[![pipeline status](https://gitlab.com/brettops/deployments/gitlab-pipeline-schedules/badges/main/pipeline.svg)](https://gitlab.com/brettops/deployments/gitlab-pipeline-schedules/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

GitLab pipeline schedules for the [`brettops` GitLab group](https://gitlab.com/brettops).

[**View the pipeline schedule**](https://brettops.gitlab.io/terraform/deployments/gitlab-pipeline-schedules/)

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | ~> 15.9.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | ~> 2.3.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.4.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 15.9.0 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.3.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.4.3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_pipeline_schedule.brettops](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule) | resource |
| [local_file.schedule_page](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [random_integer.hour](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [random_integer.weekday](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [gitlab_group.brettops](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_projects.brettops](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/projects) | data source |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_pipeline_schedules"></a> [pipeline\_schedules](#output\_pipeline\_schedules) | n/a |
| <a name="output_projects"></a> [projects](#output\_projects) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->
