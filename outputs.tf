output "projects" {
  value = local.projects
}


output "pipeline_schedules" {
  value = local.pipeline_schedules
}
