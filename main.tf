resource "random_integer" "hour" {
  for_each = local.projects
  min      = 0
  max      = 23
}

resource "random_integer" "weekday" {
  for_each = local.projects
  min      = 0
  max      = 6
}

resource "gitlab_pipeline_schedule" "brettops" {
  for_each = local.pipeline_schedules

  project     = each.key
  ref         = each.value.project.ref
  cron        = "0 ${each.value.hour} * * ${each.value.weekday}"
  description = "Terraform-managed weekly run to exercise pipelines"
}

resource "local_file" "schedule_page" {
  content = templatefile("${path.module}/schedule.html.tpl", {
    schedule = local.schedule_page
  })
  filename        = "${path.module}/public/index.html"
  file_permission = "0644"
}
